namespace ShelterMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdoptionContracts",
                c => new
                    {
                        AdoptionContractID = c.Int(nullable: false, identity: true),
                        MyProperty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdoptionContractID);
            
            CreateTable(
                "dbo.TempHomes",
                c => new
                    {
                        TempHomeID = c.Int(nullable: false, identity: true),
                        Adress = c.String(),
                    })
                .PrimaryKey(t => t.TempHomeID);
            
            AddColumn("dbo.Animals", "OutsideShelter", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Animals", "OutsideShelter");
            DropTable("dbo.TempHomes");
            DropTable("dbo.AdoptionContracts");
        }
    }
}
