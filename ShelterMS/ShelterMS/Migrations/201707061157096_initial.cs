namespace ShelterMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        AnimalID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PreviousName = c.String(),
                        diet = c.String(),
                        DecaesedReason = c.String(),
                        Decaesed = c.DateTime(),
                        InTake = c.DateTime(),
                        WellCheck = c.DateTime(),
                        CreatedTime = c.DateTime(),
                        UpdatedTime = c.DateTime(),
                        Neuter = c.Boolean(nullable: false),
                        Birthday = c.DateTime(),
                        SpeciesID = c.Int(nullable: false),
                        GenderID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AnimalID)
                .ForeignKey("dbo.Genders", t => t.GenderID, cascadeDelete: true)
                .ForeignKey("dbo.Species", t => t.SpeciesID, cascadeDelete: true)
                .Index(t => t.SpeciesID)
                .Index(t => t.GenderID);
            
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        GenderID = c.Int(nullable: false, identity: true),
                        GenderType = c.String(),
                    })
                .PrimaryKey(t => t.GenderID);
            
            CreateTable(
                "dbo.Species",
                c => new
                    {
                        SpeciesID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SpeciesID);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        NoteID = c.Int(nullable: false, identity: true),
                        Detail = c.String(),
                        AnimalID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NoteID)
                .ForeignKey("dbo.Animals", t => t.AnimalID, cascadeDelete: true)
                .Index(t => t.AnimalID);
            
            CreateTable(
                "dbo.Shots",
                c => new
                    {
                        ShotID = c.Int(nullable: false, identity: true),
                        ShotTypeID = c.Int(nullable: false),
                        AnimalID = c.Int(nullable: false),
                        ShotTime = c.DateTime(nullable: false),
                        ExpireTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ShotID)
                .ForeignKey("dbo.Animals", t => t.AnimalID, cascadeDelete: true)
                .ForeignKey("dbo.ShotTypes", t => t.ShotTypeID, cascadeDelete: true)
                .Index(t => t.ShotTypeID)
                .Index(t => t.AnimalID);
            
            CreateTable(
                "dbo.ShotTypes",
                c => new
                    {
                        ShotTypeID = c.Int(nullable: false, identity: true),
                        Detail = c.String(),
                    })
                .PrimaryKey(t => t.ShotTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shots", "ShotTypeID", "dbo.ShotTypes");
            DropForeignKey("dbo.Shots", "AnimalID", "dbo.Animals");
            DropForeignKey("dbo.Notes", "AnimalID", "dbo.Animals");
            DropForeignKey("dbo.Animals", "SpeciesID", "dbo.Species");
            DropForeignKey("dbo.Animals", "GenderID", "dbo.Genders");
            DropIndex("dbo.Shots", new[] { "AnimalID" });
            DropIndex("dbo.Shots", new[] { "ShotTypeID" });
            DropIndex("dbo.Notes", new[] { "AnimalID" });
            DropIndex("dbo.Animals", new[] { "GenderID" });
            DropIndex("dbo.Animals", new[] { "SpeciesID" });
            DropTable("dbo.ShotTypes");
            DropTable("dbo.Shots");
            DropTable("dbo.Notes");
            DropTable("dbo.Species");
            DropTable("dbo.Genders");
            DropTable("dbo.Animals");
        }
    }
}
