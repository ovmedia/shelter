﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShelterMS.Models;

namespace ShelterMS.Controllers
{
    public class ShotsController : Controller
    {
        private ShelterContext db = new ShelterContext();

        // GET: Shots
        public ActionResult Index()
        {
            var shots = db.Shots.Include(s => s.Animals).Include(s => s.ShotTypes);
            return View(shots.ToList());
        }

        // GET: Shots/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shot shot = db.Shots.Find(id);
            if (shot == null)
            {
                return HttpNotFound();
            }
            return View(shot);
        }

        // GET: Shots/Create
        public ActionResult Create()
        {
            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name");
            ViewBag.ShotTypeID = new SelectList(db.ShotTypes, "ShotTypeID", "Detail");
            return View();
        }

        // POST: Shots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShotID,ShotTypeID,AnimalID,ShotTime,ExpireTime")] Shot shot)
        {
            if (ModelState.IsValid)
            {
                db.Shots.Add(shot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", shot.AnimalID);
            ViewBag.ShotTypeID = new SelectList(db.ShotTypes, "ShotTypeID", "Detail", shot.ShotTypeID);
            return View(shot);
        }

        // GET: Shots/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shot shot = db.Shots.Find(id);
            if (shot == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", shot.AnimalID);
            ViewBag.ShotTypeID = new SelectList(db.ShotTypes, "ShotTypeID", "Detail", shot.ShotTypeID);
            return View(shot);
        }

        // POST: Shots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShotID,ShotTypeID,AnimalID,ShotTime,ExpireTime")] Shot shot)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", shot.AnimalID);
            ViewBag.ShotTypeID = new SelectList(db.ShotTypes, "ShotTypeID", "Detail", shot.ShotTypeID);
            return View(shot);
        }

        // GET: Shots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shot shot = db.Shots.Find(id);
            if (shot == null)
            {
                return HttpNotFound();
            }
            return View(shot);
        }

        // POST: Shots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Shot shot = db.Shots.Find(id);
            db.Shots.Remove(shot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
