﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShelterMS.Startup))]
namespace ShelterMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
