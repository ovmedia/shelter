﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class ShotType
    {
        public int ShotTypeID { get; set; }
        public string Detail { get; set; }
    }
}