﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class Species
    {
        public int SpeciesID { get; set; }
        public string Name { get; set; }
    }
}