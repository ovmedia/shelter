﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class Note
    {   [Key]
        public int NoteID { get; set; }
        public string Detail { get; set; }
        [ForeignKey("Animals")]
        public int AnimalID { get; set; }
        public virtual Animal Animals { get; set; }
    }
}