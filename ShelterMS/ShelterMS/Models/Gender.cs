﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class Gender
    {
        public int GenderID { get; set; }
        public string GenderType { get; set; }
    }
}