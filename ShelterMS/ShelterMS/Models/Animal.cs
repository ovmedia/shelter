﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShelterMS.Models
{
    public class Animal
    {
        [Key]
        public int AnimalID { get; set; }
        public string Name { get; set; }
        public string PreviousName { get; set; }
        public string diet { get; set; }
        public string DecaesedReason { get; set; }
        public Nullable<DateTime> Decaesed { get; set; }
        public Nullable<DateTime> InTake { get; set; }
        public Nullable<DateTime> WellCheck { get; set; }
        public Nullable<DateTime> CreatedTime { get; set; }
        public Nullable<DateTime> UpdatedTime { get; set; }
        public Boolean Neuter { get; set; }
        public Nullable<DateTime> Birthday { get; set; }
        //public Species Species { get; set; }
        //   [ForeignKey("Species")]
        // public int SpeciesID { get; set; }
        public Boolean OutsideShelter { get; set; }



        [ForeignKey("Species")]
        public int SpeciesID { get; set; }
        public virtual Species Species { get; set; }

        [ForeignKey("Genders")]
        public int GenderID { get; set; }
        public virtual Gender Genders { get; set; }


        public Animal()
        {
            CreatedTime = DateTime.UtcNow;
            UpdatedTime = DateTime.UtcNow;
        }



    }

}