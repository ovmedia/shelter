﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class Shot
    {
        public int ShotID { get; set; }
        [ForeignKey("ShotTypes")]
        public int ShotTypeID { get; set; }
        public virtual ShotType ShotTypes { get; set; }

        [ForeignKey("Animals")]
        public int AnimalID { get; set; }
        public virtual Animal Animals { get; set; }

        public DateTime ShotTime { get; set; }
        public Nullable<DateTime> ExpireTime { get; set; }
    }
}