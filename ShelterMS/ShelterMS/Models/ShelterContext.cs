﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace ShelterMS.Models
{
    public class ShelterContext : DbContext
    {
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Species> Species { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<ShotType> ShotTypes { get; set; }
        public DbSet<Shot> Shots { get; set; }
        public DbSet<TempHome> TempHomes { get; set; }
        public DbSet<AdoptionContract> AdoptionContracts { get; set; }


    }

}