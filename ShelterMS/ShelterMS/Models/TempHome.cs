﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShelterMS.Models
{
    public class TempHome
    {
        public int TempHomeID { get; set; }
        public string Adress { get; set; }
    }
}